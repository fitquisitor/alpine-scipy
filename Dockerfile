FROM jhohman/numpy:py3.6-alpine

LABEL maintainer="James Hohman <jhohman@fitquisitor.com>"

# Add our build-time dependencies for removal later.
RUN apk add --update-cache --no-cache --virtual .build-time-dependencies \
  build-base \
  libgcc \
  libquadmath \
  musl \
  libgfortran \
  gfortran \
  lapack-dev \
    && pip install --no-cache-dir scipy~=0.14.0 \
    && find /usr/local \
        \( -type d -a -name test -o -name tests \) \
        -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
        -exec rm -rf '{}' + \
    # Determine which packages are run-time dependencies
    && runDeps="$( \
        scanelf --needed --nobanner --recursive /usr/local \
                | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
                | sort -u \
                | xargs -r apk info --installed \
                | sort -u \
    )" \
    && apk add --virtual .run-time-dependencies ${runDeps} \
    # Remove build-time dependencies
    && apk del .build-time-dependencies \
    && rm -rf /root/.cache
